FROM smithmicro/web2py

USER root

# Copy my applications to the image
#COPY applications $WEB2PY_ROOT/applications/
COPY . $WEB2PY_ROOT

RUN yum install -y postgresql-devel openldap-devel
#RUN apt-get update && apt-get install -y postgresql-client

RUN pip install -r $WEB2PY_ROOT/applications/pips/requirements.txt

# Install required Python pacakages, remove sample apps and set proper ownership
RUN pip install fileutils pillow requests \
 && rm -rf $WEB2PY_ROOT/applications/welcome $WEB2PY_ROOT/applications/examples \
 && chown -R web2py:web2py $WEB2PY_ROOT

USER web2py

