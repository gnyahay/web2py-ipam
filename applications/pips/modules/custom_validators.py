# -*- coding: utf-8 -*-

import re
import netaddr
import sys
#from IPy import IP
#import logging

#from globals import current
#logger = current.logger

from validators import Validator


class IS_VALID_OWNER(Validator):
    def __init__(self, dbset, table_name, regex_field, id, error_message='Owner does not match regex'):
        self.dbset = dbset
        self.table_name = table_name
        self.regex_field = regex_field
        self.id = id
        self.e = error_message

    def __call__(self, value):
        #logger.debug("Value: %s - ID: %s" % (value, self.id))
        if not value and not self.id:
            return (value, None)
        row = self.dbset(self.dbset[self.table_name].id==self.id).select(self.regex_field).first()
        if not row or not row.regex:
            self.e = "Referenced %s id not found" % self.table_name
            return (value, self.e)
        regex = '^%s$' % (row.regex)
        #msg = "Regex: {0} - Value: {1}".format(regex, value)
        #logger.debug(msg)
        regex = re.compile(regex)
        match = regex.match(value)
        if match:
            return (value, None)
        return (value, self.e)


class IS_IP_IN_NETBLOCK(Validator):
    def __init__(self, dbset, netblock_id, error_message='IP Address is not in parent netblock!'):
        self.dbset = dbset
        self.netblock_id = netblock_id
        self.e = error_message

    def __call__(self, value):
        db = self.dbset
        allocation_netblock_type_id = db(db.netblock_type.description=='allocation').select(db.netblock_type.id).first()['id']
        network_address = db((db.netblock.id==self.netblock_id)&(db.netblock.netblock_type_id==allocation_netblock_type_id)).select(db.netblock.networkaddress).first()['networkaddress']
        #logger.debug("Network Address: {0} - NB Type ID: {1}".format(network_address, allocation_netblock_type_id))
        print "Network Address: {0} - NB Type ID: {1} - IP: {2}".format(network_address, allocation_netblock_type_id, value)

        if netaddr.IPAddress(value) in netaddr.IPNetwork(network_address):
            return (value, None)
        return (value, self.e)


class IS_VALID_IP(Validator):
    def __init__(self, error_message='IP Address is not valid!'):
        self.e = error_message

    def __call__(self, value):
        try:
            if netaddr.IPAddress(value):
                return (value, None)
        except netaddr.AddrFormatError as detail:
            return (value, detail)
        except:
            return (value, sys.exc_info()[0])