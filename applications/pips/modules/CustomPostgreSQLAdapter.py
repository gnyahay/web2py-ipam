#!/usr/bin/env python
# coding: utf8
from gluon.dal import ADAPTERS, PostgreSQLAdapter


class CustomPostgreSQLAdapter(PostgreSQLAdapter):
    drivers = ('psycopg2', 'pg8000')

    QUOTE_TEMPLATE = '"%s"'

    support_distributed_transaction = True
    types = {'boolean': 'CHAR(1)',
             'string': 'VARCHAR(%(length)s)',
             'text': 'TEXT',
             'json': 'TEXT',
             'password': 'VARCHAR(%(length)s)',
             'blob': 'BYTEA',
             'upload': 'VARCHAR(%(length)s)',
             'integer': 'INTEGER',
             'bigint': 'BIGINT',
             'float': 'FLOAT',
             'double': 'FLOAT8',
             'decimal': 'NUMERIC(%(precision)s,%(scale)s)',
             'date': 'DATE',
             'time': 'TIME',
             'datetime': 'TIMESTAMP',
             'id': 'SERIAL PRIMARY KEY',
             'reference': 'INTEGER REFERENCES %(foreign_key)s ON DELETE %(on_delete_action)s',
             'list:integer': 'TEXT',
             'list:string': 'TEXT',
             'list:reference': 'TEXT',
             'geometry': 'GEOMETRY',
             'geography': 'GEOGRAPHY',
             'big-id': 'BIGSERIAL PRIMARY KEY',
             'big-reference': 'BIGINT REFERENCES %(foreign_key)s ON DELETE %(on_delete_action)s',
             'reference FK': ', CONSTRAINT  "FK_%(constraint_name)s" FOREIGN KEY (%(field_name)s) REFERENCES %(foreign_key)s ON DELETE %(on_delete_action)s',
             'reference TFK': ' CONSTRAINT  "FK_%(foreign_table)s_PK" FOREIGN KEY (%(field_name)s) REFERENCES %(foreign_table)s (%(foreign_key)s) ON DELETE %(on_delete_action)s',
             'cidr': 'cidr',
             'inet': 'inet',
             'macaddr': 'macaddr'
             }

ADAPTERS.update( {
    'postgrescustom': CustomPostgreSQLAdapter
})
