# -*- coding: utf-8 -*-

## if SSL/HTTPS is properly configured and you want all HTTP requests to
## be redirected to HTTPS, uncomment the line below:
#request.requires_https()

from IPy import IP
import logging
logger = logging.getLogger("web2py.app.connect_w2p")
logger.setLevel(logging.DEBUG)

import CustomPostgreSQLAdapter

db = DAL('postgrescustom://pimps:johnson@127.0.0.1:5432/pimps',
         pool_size=20,
         check_reserved=['postgres'],
         #lazy_tables=True,
         #fake_migrate_all=True,
         #migrate_all=True,
         #migrate=True
         )

## by default give a view/generic.extension to all actions from localhost
## none otherwise. a pattern can be 'controller/function.extension'
#response.generic_patterns = ['*'] if request.is_local else []
# Override to get crud working
response.generic_patterns = ['*']


session.titleargs = None

from gluon.tools import Auth, Crud, Service, PluginManager, prettydate
# Eventually turn off csrf_prevention = False
#auth = Auth(db,secure=True, csrf_prevention=False)
auth = Auth(db,secure=False)
crud, service, plugins = Crud(db), Service(), PluginManager()

#from gluon.contrib.login_methods.ldap_auth import ldap_auth
# from ldap_auth_ad import ldap_auth
# auth.settings.login_methods.append(ldap_auth(
#     mode='ad',
#     server='192.168.16.6',
#     base_dn='dc=icenet,dc=local',
#     secure=True,
#     bind_dn='oakproxy',
#     bind_pw='XXXXX',
#     manage_groups=True,
#     manage_user=True,
#     db=db,
#     user_firstname_attrib='cn:1',
#     user_lastname_attrib='cn:2',
#     user_mail_attrib='mail',
#     group_dn='ou=Corporate Groups,dc=icenet,dc=local',
#     group_name_attrib='cn',
#     group_member_attrib='memberUid',
#     group_filterstr='objectClass=*',
#     logging_level='debug'
#     #allowed_groups=['UNIX-IT','Networking','UNIX-DEV'],
#     #cert_file="/etc/openldap/certs/corp_ca.crt"
#     ))

## create all tables needed by auth if not custom tables
auth.define_tables(username=True, signature=True)

## configure email
mail = auth.settings.mailer
mail.settings.server = 'logging' or 'smtp.gmail.com:587'
mail.settings.sender = 'you@gmail.com'
mail.settings.login = 'username:password'

## configure auth policy
auth.settings.registration_requires_verification = False
auth.settings.registration_requires_approval = False
auth.settings.reset_password_requires_verification = False
# 'profile'
# If LDAP is enabled disable these things...
#auth.settings.actions_disabled = ['register','request_reset_password','retrieve_username','change_password']
auth.settings.actions_disabled = []

from custom_validators import IS_VALID_OWNER, IS_IP_IN_NETBLOCK, IS_VALID_IP
from gluon.dal import SQLCustomType


def empty_string_to_none(x):
    if x == "":
        x = None
    return x

cidr = SQLCustomType(
    type='string',
    native='cidr',
    # encoder=(lambda x: repr(str(x))),
    # decoder=(lambda x: repr(str(x)))
)
inet = SQLCustomType(
    type='string',
    native='inet',
    encoder=(lambda x: x),
    decoder=(lambda x: x)
)
# Must use custom encoder for macaddr otherwise postgres driver throws an error for ""
macaddr = SQLCustomType(
    type='string',
    native='macaddr',
    encoder=(lambda x: empty_string_to_none(x)),
    decoder=(lambda x: x)
)

db._common_fields.append(auth.signature)

generic_type = db.Table(db, 'generic_type',
    Field('description', 'text', required=True, notnull=True, unique=True, widget=SQLFORM.widgets.string.widget),
    format='%(description)s')

# db.define_table('settings',
#                 Field('value', 'text', required=True, notnull=True),
#                 generic_type)

# Not sure about required.....
db.define_table('attribute',
                Field('description', 'text', required=True, notnull=True, unique=True, comment='Friendly/Display name for field', widget=SQLFORM.widgets.string.widget),
                Field('field_name', 'text', required=True, notnull=True, unique=True, comment='API name for field', widget=SQLFORM.widgets.string.widget),
                # This field probably isn't necessary
                Field('required', 'boolean', required=True, notnull=True, default=True),
                Field('regex', 'text', label='Regular Expression', comment='A regular expression applied to data upon input', widget=SQLFORM.widgets.string.widget),
                Field('inheritable', 'boolean', required=True, notnull=True, default=False, comment='Defines whether related child objects will include in API results'),
                Field('require_unique', 'boolean', required=True, notnull=True, default=False, comment='Defines whether or not multiple of the same attribute can exist on an entity'))

db.define_table('owner_type',
                Field('field_name', 'text', required=True, notnull=True, unique=True, widget=SQLFORM.widgets.string.widget),
                Field('regex', 'text', label='Regular Expression', comment='A regular expression applied to data upon input', widget=SQLFORM.widgets.string.widget),
                generic_type)
#if db(db.owner_type).isempty():
# if cache.ram('init',lambda:db(db.owner_type).isempty(),None):
#     db.owner_type.bulk_insert([
#         {'description':'Client ID', 'field_name':'client_id', 'regex':'\d+'},
#         {'description':'Service Name', 'field_name':'service_name', 'regex':'[a-zA-Z0-9-]+'},
#         {'description':'Service ID', 'field_name':'service_id', 'regex':'\d+'},
#         {'description':'Solution ID', 'field_name':'solution_id', 'regex':'\d+'},
#         {'description':'Salt UUID', 'field_name':'salt_uuid', 'regex':'[a-zA-Z0-9\.-]+'}])

db.define_table('location',
                Field('dc_abbr', 'text', required=True, notnull=True, unique=True, label='Abbreviation', widget=SQLFORM.widgets.string.widget),
                generic_type)

db.define_table('network_segment',
        Field('location_id', db.location, label='Location'),
        generic_type)
db.network_segment.location_id.requires = IS_IN_DB(db, 'location.id', '%(description)s', zero=T('choose one'))

db.define_table('vlan_type', generic_type)

db.define_table('vlan',
                Field('vnumber', 'integer', label='VLAN Number'),
                Field('vni', 'integer', required=False, label='VXLAN ID Number'),
                Field('vlan_type_id', db.vlan_type, label='VLAN Type'),
                Field('network_segment_id', db.network_segment, label='Network Segment'),
                Field('description', 'text', widget=SQLFORM.widgets.string.widget),
                Field('owner', 'text', widget=SQLFORM.widgets.string.widget),
                Field('owner_type_id', db.owner_type, label='Owner Type'))
                #Field('client_id', 'integer', label='Client ID'))
db.vlan.vlan_type_id.requires = IS_IN_DB(db, 'vlan_type.id', '%(description)s', zero=T('choose one'))
db.vlan.network_segment_id.requires = IS_NULL_OR(IS_IN_DB(db, 'network_segment.id', '%(description)s', zero=T('choose one')))
db.vlan.owner_type_id.requires = IS_NULL_OR(IS_IN_DB(db, 'owner_type.id', '%(description)s', zero=T('choose one')))
db.vlan.owner.requires = IS_VALID_OWNER(db, 'owner_type', 'regex', request.vars.owner_type_id)

db.define_table('vlan_attribute',
                Field('vlan_id', db.vlan, label='VLAN'),
                Field('attribute_id', db.attribute, label='Attribute'),
                Field('value', 'text', widget=SQLFORM.widgets.string.widget))
db.vlan_attribute.vlan_id.requires = IS_IN_DB(db, 'vlan.id', '%(description)s', zero=T('choose one'))
db.vlan_attribute.attribute_id.requires = IS_IN_DB(db, 'attribute.id', '%(description)s', zero=T('choose one'))

db.define_table('netblock_type', generic_type)
#if db(db.netblock_type).isempty():
# if cache.ram('init',lambda:db(db.netblock_type).isempty(),None):
#     db.netblock_type.bulk_insert([
#         {'description':'root'},
#         {'description':'aggregate'},
#         {'description':'allocation'}])

# Not sure if we need this table or not
#db.define_table('netblock_status', generic_type)

db.define_table('netblock',
                Field('networkaddress', type=cidr, required=True, notnull=True, label='Network Address'),
                Field('vlan_id', db.vlan, default=None, label='VLAN'),
                Field('netblock_type_id', db.netblock_type, label='Netblock Type'),
                Field('network_segment_id', db.network_segment, label='Network Segment'),
                # May not need this field...
                #Field('gateway', type=inet),
                # TODO: This field doesnt work right or im forgetting something about it
                Field('base', 'boolean', default=True, label='Allow Primary IPs'),
                Field('owner', 'text', widget=SQLFORM.widgets.string.widget),
                Field('owner_type_id', db.owner_type, label='Owner Type'),
                format='%(networkaddress)s')    
                #Field('client_id', 'integer', label='Client ID'))
db.netblock.vlan_id.requires = requires = IS_NULL_OR(IS_IN_DB(db,'vlan.id', '%(description)s - %(vnumber)s'))
db.netblock.netblock_type_id.requires = IS_IN_DB(db,'netblock_type.id', '%(description)s', zero=T('choose one'))
db.netblock.network_segment_id.requires = IS_NULL_OR(IS_IN_DB(db, 'network_segment.id', '%(description)s', zero=T('choose one')))
db.netblock.owner_type_id.requires = IS_NULL_OR(IS_IN_DB(db, 'owner_type.id', '%(description)s', zero=T('choose one')))
db.netblock.owner.requires = IS_VALID_OWNER(db, 'owner_type', 'regex', request.vars.owner_type_id)

#db.netblock.networkaddress.requires = IS_NOT_IN_DB()
# If you insert a root, aggregate or allocation block no existing block of the same type should contain it or equal to it
#   RFC 1918 aggregate blocks may overlap as long as network_segment_id is different
#   RFC 1918 allocation blocks may overlap as long as vlan_id is different
# root types: base = null, vlan_id = null, network_segment_id = null, owner= null, owner_type_id = null
# aggregate types: base = null, vlan_id = null, owner= null, owner_type_id = null - network_segment_id should be populated
# allocation types: base = true or false, vlan_id should not be null, network_segment_id should be null

# Changing a netblock type to root or aggregate should enforce that no related IP addresses exist in IP address table

# Inserting an allocation netblock should create network address and broadcast address in IP Address table (possibly gateway) DONE
# deleting a netblock with entries in ip address table should either cascase or prevent delete

db.define_table('netblock_attribute',
                Field('netblock_id', db.netblock, label='Netblock'),
                Field('attribute_id', db.attribute, label='Attribute'),
                Field('value', 'text', widget=SQLFORM.widgets.string.widget))
db.netblock_attribute.netblock_id.requires = IS_IN_DB(db, 'netblock.id', '%(networkaddress)s', zero=T('choose one'))
db.netblock_attribute.attribute_id.requires = IS_IN_DB(db, 'attribute.id', '%(description)s', zero=T('choose one'))

db.define_table('ip_address_use_type', generic_type)
#if db(db.ip_address_use_type).isempty():
# if cache.ram('init',lambda:db(db.ip_address_use_type).isempty(),None):
#     db.ip_address_use_type.bulk_insert([
#         {'description':'network address'},
#         {'description':'broadcast address'},
#         {'description':'gateway'},
#         {'description':'reserved'},
#         {'description':'unknown'},
#         {'description':'ipmi'},
#         {'description':'nat'},
#         {'description':'management'},
#         {'description':'primary'},
#         {'description':'secondary'}])

# May not need this table...
#db.define_table('ip_address_status', generic_type)

db.define_table('ip_address',
                Field('id', 'big-id'),
                Field('ip', type=inet, notnull=True, label='IP Address'),
                Field('netblock_id', db.netblock, label='Parent Netblock'),
                Field('service_name', 'text', label='Service Name', widget=SQLFORM.widgets.string.widget),
                Field('hostname', 'text', widget=SQLFORM.widgets.string.widget),
                Field('description', 'text', widget=SQLFORM.widgets.string.widget),
                Field('arp', type=macaddr, notnull=False, required=False, label='MAC/ARP Address'),
                # FYI - Removed not null and required=True,- could be null if discovery found it and doesn't know...
                Field('is_primary', 'boolean', default=False, label='Is Primary IP'),
                Field('ip_address_use_type_id', db.ip_address_use_type, label='IP Use Type'),
                Field('owner', 'text', widget=SQLFORM.widgets.string.widget),
                Field('owner_type_id', db.owner_type, label='Owner Type'))
db.ip_address.netblock_id.requires = IS_IN_DB(db, 'netblock.id', '%(networkaddress)s', zero=T('choose one'))
db.ip_address.owner_type_id.requires = IS_NULL_OR(IS_IN_DB(db, 'owner_type.id', '%(description)s', zero=T('choose one')))
db.ip_address.owner.requires = IS_VALID_OWNER(db, 'owner_type', 'regex', request.vars.owner_type_id)
db.ip_address.ip_address_use_type_id.requires = IS_IN_DB(db, 'ip_address_use_type.id', '%(description)s', zero=T('choose one'))
db.ip_address.ip.requires = [IS_VALID_IP(), IS_IP_IN_NETBLOCK(db, request.vars.netblock_id)]

# create unique index ip_address_is_active_ip_netblock_id_idx ON ip_address (is_active, ip, netblock_id);

# IP addresses must point at a netblock of type allocation
# If base is true then the parent netblock's base field must also be true

db.define_table('ip_address_attribute',
                Field('id', 'big-id'),
                Field('ip_address_id', db.ip_address),
                Field('attribute_id', db.attribute),
                Field('value', 'text', widget=SQLFORM.widgets.string.widget))
db.ip_address_attribute.ip_address_id.requires = IS_IN_DB(db, 'ip_address.id', '%(ip)s', zero=T('choose one'))
db.ip_address_attribute.attribute_id.requires = IS_IN_DB(db, 'attribute.id', '%(description)s', zero=T('choose one'))

#db.define_table('ip_address_arp',
#                Field('id', 'big-id'),
#                Field('ip_address_id', db.ip_address),
#                Field('arp', type=macaddr, notnull=True, required=False),
                #Field('last_seen', 'timestamp', notnull=True, required=True),
#                Field('vlan_id', requires=IS_NULL_OR(IS_IN_DB(db,'vlan.id')), default=None),
#                Field('router_id', requires=IS_NULL_OR(IS_IN_DB(db,'router.id')), default=None))

# db.define_table('router',
#                 Field('ip_address_id', db.ip_address),
#                 Field('description', 'text'))
#
# db.define_table('routes',
#                 Field('router_id',db.router),
#                 Field('networkaddress', type=cidr, required=True, notnull=True),
#                 Field('destination', type=inet, required=True, notnull=True),
#                 Field('last_seen', 'datetime', notnull=True, required=True))

## after defining tables, uncomment below to enable auditing
auth.enable_record_versioning(db)


#def before_insert_netblock(f):
def after_insert_netblock(f, id):
    netblock_type = db(db.netblock_type.id==f.netblock_type_id).select(db.netblock_type.description).first()['description']
    logger.debug("Netblock Type: {0} - NB ID: {1} - Row: {2}".format(netblock_type, id, f))

    if netblock_type == 'allocation':
        ipnw = IP(f.networkaddress)
        version = ipnw.version()
        network_address = ipnw.net().strCompressed()
        network_address_id = db(db.ip_address_use_type.description=='network address').select(db.ip_address_use_type.id).first()['id']
        broadcast_address = ipnw.broadcast().strCompressed()
        broadcast_address_id = db(db.ip_address_use_type.description=='broadcast address').select(db.ip_address_use_type.id).first()['id']

        logger.debug("IP Version: {0} - NA: {1} - BA: {2} - NA ID: {3} - BC ID: {4}".format(version, network_address, broadcast_address, network_address_id, broadcast_address_id))

        # Insert network and broadcast addresses in ip_address
        db.ip_address.bulk_insert([
            {'netblock_id':id, 'is_primary':True, 'ip':network_address, 'ip_address_use_type_id':network_address_id},
            {'netblock_id':id, 'is_primary':True, 'ip':broadcast_address, 'ip_address_use_type_id':broadcast_address_id}])

#db.netblock._before_insert.append(lambda f: pprint(f))
db.netblock._after_insert.append(lambda f,id: after_insert_netblock(f,id))
#db.person._before_update.append(lambda s,f: pprint(s,f))
#db.person._after_update.append(lambda s,f: pprint(s,f))
#db.person._before_delete.append(lambda s: pprint(s))
#db.netblock._after_delete.append(lambda s: pprint(s))


# This should give creator of something permissions to it
def give_create_permission(form):
    group_id = auth.id_group('user_%s' % auth.user.id)
    auth.add_permission(group_id, 'read', db.ip_address,0)
    auth.add_permission(group_id, 'create', db.ip_address,0)
    auth.add_permission(group_id, 'select', db.ip_address,0)

#def give_update_permission(form):
#    test_id=form.vars.id
#    group_id = auth.id_group('user_%s' % auth.user.id)
#    auth.add_permission(group_id,'update',db.test,0)
#    auth.add_permission(group_id,'delete',db.test,0)
   
auth.settings.register_onaccept=give_create_permission


# Menu item for the following: URL('appadmin','manage',args=['db_admin'])
auth.settings.manager_actions = dict(db_admin=dict(role='Admin', heading='Manage Database', tables=db.tables))

# Enable this as-needed
auth.settings.allow_basic_login = True

# REVERT THIS ONCE AUTHENTICATION IS FIGURED OUT!!!
#crud.settings.auth = auth
crud.settings.auth = None
