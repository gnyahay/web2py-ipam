# -*- coding: utf-8 -*-


def get_table_history_grid(db, table, id):
    #record = db[table](id)

    table_history = "{0}_archive".format(table)

    db[table_history].id.readable = False
    db[table_history].current_record.readable = False

    db[table_history].created_on.readable = True
    db[table_history].created_by.readable = True
    db[table_history].modified_on.readable = True
    db[table_history].modified_by.readable = True

    history_query = ((db[table_history].current_record==id))

    history_form = SQLFORM.grid(query=history_query,
                                field_id=db[table_history].id,
                                maxtextlength=64,
                                paginate=128,
                                create=False,
                                editable=False,
                                deletable=False,
                                searchable=False,
                                details=False)
    return history_form


def get_attributes_grid(db, table, id):

    table_attributes = "{0}_attribute".format(table)

    db[table_attributes].created_on.readable = True
    db[table_attributes].created_by.readable = True
    db[table_attributes].modified_on.readable = True
    db[table_attributes].modified_by.readable = True

    attributes = db(
            (db.attribute.id==db[table_attributes].attribute_id) &
            (db[table].id==db[table_attributes]["{0}_id".format(table)]) &
            (db[table].id==id)
            #(db.attribute.is_active=='T') &
            #(db[table_attributes].is_active=='T')
        ).select(db.attribute.description, db.attribute.field_name, db[table_attributes].value)
    headers = {'attribute.description':'Description', 'attribute.field_name':'Field', "{0}.value".format(table_attributes):'Value'}
    table_attributes = SQLTABLE(attributes, headers=headers)

    return table_attributes


def get_table_attributes_grid(db, table, id):

    table_attributes = "{0}_attribute".format(table)

    db[table_attributes].created_on.readable = True
    db[table_attributes].created_by.readable = True
    db[table_attributes].modified_on.readable = True
    db[table_attributes].modified_by.readable = True

    #attribute_id = "{0}_id".format(table)
    table_id = "{0}_id".format(table)

    attribute_query = (
        (db.attribute.id==db[table_attributes].attribute_id) &
            (db[table].id==db[table_attributes][table_id]) &
            (db[table].id==id)
    )

    attribute_form = SQLFORM.grid(query=attribute_query,
                                field_id=db[table_attributes].id,
                                maxtextlength=64,
                                paginate=128,
                                create=True,
                                editable=True,
                                deletable=True,
                                searchable=False,
                                details=True)

    return attribute_form