# -*- coding: utf-8 -*-


def index():
    #db.vlan.id.readable = False
    db.location.description.label = "Location"
    db.network_segment.description.label = "NetSeg"
    db.vlan.description.label = "VLAN Description"
    db.vlan_type.description.label = "VLAN Type"
    db.owner_type.description.label = "Owner Type"

    form_query=(
        (db.network_segment.location_id==db.location.id) &
        (db.vlan.network_segment_id==db.network_segment.id) &
        (db.vlan_type.id == db.vlan.vlan_type_id)
    )
    left=[db.owner_type.on(db.owner_type.id == db.vlan.owner_type_id)]

    fields = (
        db.vlan.vnumber,
        db.vlan.description,
        db.vlan_type.description,
        db.network_segment.description,
        db.location.description,
        db.vlan.owner,
        db.owner_type.description
    )

    default_sort_order=[db.vlan.vnumber]

    form = SQLFORM.grid(query=form_query, orderby=default_sort_order, fields=fields, left=left,
                create=True, deletable=False, editable=True, maxtextlength=64, paginate=128,
                field_id=db.vlan.id)

    return dict(form=form)


def freeVlan():
    if 'network_segment_id' in request.args:
        network_segment_id = request.args.network_segment_id
    elif 'network_segment_id' in request.vars:
        network_segment_id = request.vars.network_segment_id
    else:
        return dict(message="Network Segment ID was not supplied!")

    # TODO: Allow these params to be supplied
    min = 1
    max = 4095
    num = 10

    sql = """SELECT sub.vnumber FROM (SELECT generate_series({1}, {2}) AS vnumber) AS sub WHERE sub.vnumber NOT IN (
SELECT v.vnumber FROM network_segment ns
JOIN vlan v ON v.network_segment_id = ns.id
WHERE ns.is_active = 'T'
AND v.is_active = 'T'
AND v.network_segment_id = {0}
AND v.vnumber >= {1}
AND v.vnumber <= {2}
) LIMIT {3}""".format(network_segment_id, min, max, num)
    results = db.executesql(sql)
    num_results = len(results)
    return locals()
