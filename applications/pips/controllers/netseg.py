# -*- coding: utf-8 -*-


def index():
    db.network_segment.id.readable = True
    db.location.description.label = "Location"
    db.network_segment.created_by.readable = True
    db.network_segment.created_on.readable = True
    db.network_segment.modified_by.readable = True
    db.network_segment.modified_on.readable = True

    form_query=((db.network_segment.location_id==db.location.id))
    #left=[db.location.on(db.location.id==db.network_segment.location_id)]

    fields = (
        db.network_segment.id,
        db.network_segment.description,
        db.location.description,
        db.location.dc_abbr
    )

    default_sort_order=[db.location.description]
    #left=left,
    form = SQLFORM.grid(query=form_query, orderby=default_sort_order, fields=fields,
                create=True, deletable=False, editable=True, maxtextlength=64, paginate=128,
                field_id=db.network_segment.id)

    return dict(form=form)
