# -*- coding: utf-8 -*-
import netaddr


def index():
    db.ip_address.id.readable = False
    db.vlan.id.readable = False

    db.location.description.label = "Location"
    db.network_segment.description.label = "NetSeg"
    db.vlan.description.label = "VLAN Description"
    db.vlan_type.description.label = "VLAN Type"
    db.owner_type.description.label = "Owner Type"
    db.ip_address_use_type.description.label = "Use Type"

    db.ip_address.created_by.readable = True
    db.ip_address.created_on.readable = True
    db.ip_address.modified_by.readable = True
    db.ip_address.modified_on.readable = True

    form_query=(
        (db.ip_address.netblock_id==db.netblock.id) &
        (db.netblock.vlan_id==db.vlan.id) &
        (db.vlan_type.id == db.vlan.vlan_type_id) &
        (db.vlan.network_segment_id==db.network_segment.id) &
        (db.network_segment.location_id==db.location.id)
    )
    left=[
        db.owner_type.on(db.owner_type.id==db.ip_address.owner_type_id),
        db.ip_address_use_type.on(db.ip_address_use_type.id==db.ip_address.ip_address_use_type_id),
    ]

    fields = (
        db.ip_address.ip,
        db.ip_address.is_primary,
        db.ip_address.service_name,
        db.ip_address.description,
        db.ip_address.hostname,
        db.netblock.networkaddress,
        db.ip_address_use_type.description,
        #db.netblock.base,
        db.vlan.vnumber,
        # db.vlan.description,
        # db.vlan_type.description,
        # db.network_segment.description,
        # db.location.description,
        db.ip_address.owner,
        db.owner_type.description
    )

    default_sort_order=[db.ip_address.ip]

    load_ajax_view = False
    form_id = ''
    ret = [item for item in request.args if item in ['update', 'edit', 'view']]
    if ret:
        load_ajax_view = True
        if request.args[1] and request.args[1] == 'ip_address' and request.args[2]:
            form_id = request.args[2]

    form = SQLFORM.grid(
        query=form_query,
        orderby=default_sort_order,
        fields=fields,
        left=left,
        create=False,
        deletable=True,
        editable=True,
        maxtextlength=64,
        paginate=128,
        field_id=db.ip_address.id,
        # Search widget was causing an exception being the default of 'default'
        #search_widget='default',
        searchable=False
    )

    return dict(form=form, load_ajax_view=load_ajax_view, form_id=form_id)



def ajaxHistory():
    id = ''
    if request.vars.key:
        query = request.vars.key.split('-')
    elif request.vars.id:
        id = request.vars.id
    elif 'ip_address' in request.args:
        id = request.args[2]
    elif request.post_vars.id:
        query[1] = request.post_vars.id
        id = query[1]
    else:
        return dict()

    print "FOUND ID: {0}".format(id)
    history_form = get_table_history_grid(db, 'ip_address', id)
    return dict(form=history_form, form_id=id)


def ajaxAttributes():
    id = ''
    if request.vars.key:
        query = request.vars.key.split('-')
    elif request.vars.id:
        id = request.vars.id
    elif 'ip_address' in request.args:
        id = request.args[2]
    elif request.post_vars.id:
        query[1] = request.post_vars.id
        id = query[1]
    else:
        return dict()

    attributes_form = get_table_attributes_grid(db, 'ip_address', id)
    return dict(form=attributes_form)


def find_ip_netblock():
    if 'ip' in request.vars:
        ip = request.vars.ip
        try:
            ipaddr = netaddr.IPAddress(ip)
            del ipaddr
        except:
            return dict(message=T("Invalid IP: {0}".format(ip)))
    else:
        return dict(message=T("You must provide an IP!"))

    sql = """SELECT * FROM netblock
        WHERE is_active = 'T'
        AND netblock_type_id = (SELECT id FROM netblock_type WHERE description = 'allocation')
        AND networkaddress >>= inet '{0}'""".format(ip)
    # Compat with restful API
    content = db.executesql(sql, as_dict=True)
    return locals()


@auth.requires_login()
def manage():
    table = request.args(0)
    if not table in db.tables(): redirect(URL('error'))
    grid = SQLFORM.grid(db[table],args=request.args[:1])
    return locals()