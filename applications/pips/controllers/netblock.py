# -*- coding: utf-8 -*-


def index():
    #db.vlan.id.readable = False
    db.netblock_type.description.label = "Netblock Type"
    db.netblock.base.label = "Base"
    db.location.description.label = "Location"
    db.network_segment.description.label = "NetSeg"
    db.vlan.description.label = "VLAN Description"
    db.vlan_type.description.label = "VLAN Type"
    db.owner_type.description.label = "Owner Type"

    form_query=((db.netblock.netblock_type_id==db.netblock_type.id))

    left=[db.owner_type.on(db.owner_type.id == db.netblock.owner_type_id),
          db.vlan.on(db.vlan.id==db.netblock.vlan_id),
          #db.network_segment.on(db.vlan.network_segment_id==db.network_segment.id),
          db.vlan_type.on(db.vlan_type.id == db.vlan.vlan_type_id),
          db.network_segment.on(db.network_segment.id==db.netblock.network_segment_id),
          db.location.on(db.network_segment.location_id==db.location.id)]

    fields = (
        db.netblock.networkaddress,
        db.netblock_type.description,
        db.netblock.base,
        db.vlan.vnumber,
        #db.vlan.description,
        db.vlan_type.description,
        db.network_segment.description,
        db.location.dc_abbr,
        db.netblock.owner,
        db.owner_type.description
    )

    default_sort_order=[db.netblock.networkaddress]

    form = SQLFORM.grid(query=form_query, orderby=default_sort_order, fields=fields, left=left,
                create=True, deletable=False, editable=True, maxtextlength=64, paginate=128,
                field_id=db.netblock.id)

    return dict(form=form)


def tree():
    return dict()


def ajax():
    if request.vars.key:
        query = request.vars.key.split('-')
    elif request.post_vars.id:
        query[1] = request.post_vars.id
    else:
        return dict()

    record = db.netblock(query[1])

    db.netblock.created_on.readable = True
    db.netblock.created_by.readable = True
    db.netblock.modified_on.readable = True
    db.netblock.modified_by.readable = True
    form = SQLFORM(db.netblock, record, deletable=True)

    if form.accepts(request):
        response.flash = 'Changes saved'
    return dict(form=form)


def ajaxIps():
    if request.vars.key:
        query = request.vars.key.split('-')
    elif request.post_vars.id:
        query[1] = request.post_vars.id
    else:
        return dict()

    db.ip_address.id.readable = False
    db.ip_address_use_type.description.label = "Use Type"

    form_query=((db.ip_address.netblock_id==query[1]))
    left=[db.ip_address_use_type.on(db.ip_address_use_type.id==db.ip_address.ip_address_use_type_id)]

    fields = (db.ip_address.ip, db.ip_address.is_primary, db.ip_address.description, db.ip_address.hostname, db.ip_address_use_type.description)

    default_sort_order=[db.ip_address.ip]

    form = SQLFORM.grid(query=form_query, left=left, orderby=default_sort_order, fields=fields,
                create=False, deletable=False, editable=False, maxtextlength=64, paginate=128,
                searchable=False)

    return dict(form=form, netblock_id=query[1])


def ajaxHistory():
    if request.vars.key:
        query = request.vars.key.split('-')
    elif request.post_vars.id:
        query[1] = request.post_vars.id
    else:
        return dict()

    history_form = get_table_history_grid(db, 'netblock', query[1])
    return dict(form=history_form)


def ajaxAttributes():
    if request.vars.key:
        query = request.vars.key.split('-')
    elif request.post_vars.id:
        query[1] = request.post_vars.id
    else:
        return dict()

    attributes_form = get_attributes_grid(db, 'netblock', query[1])
    return dict(form=attributes_form)


def treeAjax():
    response.view = 'treeAjax.json'
    netblock_type = 'root'
    if request.vars['key']:
        query = request.vars.key.split('-')
        if query[0] == 'root':
            netblock_type = 'aggregate'
        else:
            netblock_type = 'allocation'

    sql = """
SELECT
    (%s || '-' || n1.id) AS key,
    n1.networkaddress AS title,
    (SELECT COUNT(*) FROM netblock n2 WHERE n2.is_active = 'T' AND n2.networkaddress <<= n1.networkaddress) AS count
FROM netblock n1
JOIN netblock_type nt ON nt.id = n1.netblock_type_id
    AND nt.is_active = 'T'
WHERE n1.is_active = 'T'
AND nt.description = %s"""

    if netblock_type != 'root':
        sql += "AND n1.networkaddress <<= (select n3.networkaddress from netblock n3 where n3.id = %s)"
        sql += "ORDER BY n1.networkaddress"
        netblocks = db.executesql(sql, placeholders=[netblock_type, netblock_type, query[1]], as_dict=True)
    else:
        sql += "ORDER BY n1.networkaddress"
        netblocks = db.executesql(sql, placeholders=[netblock_type, netblock_type], as_dict=True)

    if netblock_type != 'allocation':
        for netblock in netblocks:
            if netblock['count'] > 0:
                netblock['lazy'] = True
                netblock['folder'] = True

    return dict(tree=netblocks)


def stats():
    if request.vars.id:
        netblock_id = request.vars.id
    elif request.vars.key:
        query = request.vars.key.split('-')
        netblock_id = query[1]
        del query
    else:
        return dict(message='Netblock ID was not supplied!')

    netblock = db((db.netblock.id==netblock_id)&(db.netblock.netblock_type_id==db.netblock_type.id)).select(db.netblock.networkaddress,db.netblock_type.description).first()
    network_address = netblock['netblock']['networkaddress']
    netblock_type = netblock['netblock_type']['description']
    logger.debug("Netblock ID: {0} - Network Address: {1} - Netblock Type: {2}".format(netblock_id,network_address,netblock_type))

    if netblock_type == 'allocation':
        sql = "SELECT" \
              " family(networkaddress) AS ip_family, " \
              " masklen(networkaddress) AS ip_masklen, " \
              " pow(2, (32 - masklen(networkaddress)))::numeric::text AS ipv4_total_ips, " \
              " pow(2, (128 - masklen(networkaddress)))::numeric::text AS ipv6_total_ips, " \
              " (SELECT COUNT(ip) FROM ip_address WHERE netblock_id = {0})::numeric::text AS total_used_ips " \
              "FROM netblock " \
              "WHERE is_active = 'T' AND id = {0}".format(netblock_id)

    elif netblock_type == 'aggregate':
        sql = "SELECT" \
              " family(n1.networkaddress) AS ip_family, " \
              " masklen(n1.networkaddress) AS ip_masklen, " \
              " pow(2, (32 - masklen(n1.networkaddress)))::numeric::text AS ipv4_total_ips, " \
              " pow(2, (128 - masklen(n1.networkaddress)))::numeric::text AS ipv6_total_ips, " \
              " (SELECT SUM(pow(2, (32 - masklen(n2.networkaddress)))::numeric) FROM netblock n2 WHERE n2.networkaddress <<= '{1}' AND n2.netblock_type_id = (SELECT nt.id FROM netblock_type nt WHERE nt.description='allocation'))::text AS total_used_ips, " \
              " (SELECT COUNT(*)::numeric FROM netblock n3 WHERE n3.networkaddress <<= '{1}' AND n3.netblock_type_id = (SELECT nt.id FROM netblock_type nt WHERE nt.description='allocation'))::text AS num_netblocks " \
              "FROM netblock n1 " \
              "WHERE is_active = 'T' AND n1.id = {0}".format(netblock_id, network_address)

    elif netblock_type == 'root':
        sql = "SELECT" \
              " family(n1.networkaddress) AS ip_family, " \
              " masklen(n1.networkaddress) AS ip_masklen, " \
              " pow(2, (32 - masklen(n1.networkaddress)))::numeric::text AS ipv4_total_ips, " \
              " pow(2, (128 - masklen(n1.networkaddress)))::numeric::text AS ipv6_total_ips, " \
              " (SELECT SUM(pow(2, (32 - masklen(n2.networkaddress)))::numeric) FROM netblock n2 WHERE n2.networkaddress <<= '{1}' AND n2.netblock_type_id = (SELECT nt.id FROM netblock_type nt WHERE nt.description='aggregate'))::text AS total_used_ips, " \
              " (SELECT COUNT(*)::numeric FROM netblock n3 WHERE n3.networkaddress <<= '{1}' AND n3.netblock_type_id = (SELECT nt.id FROM netblock_type nt WHERE nt.description='aggregate'))::text AS num_netblocks " \
              "FROM netblock n1 " \
              "WHERE is_active = 'T' AND n1.id = {0}".format(netblock_id, network_address)

    results = db.executesql(sql, as_dict = True)
    logger.debug(db._lastsql)

    if results[0]:
        results = results[0]

    if results['ip_family'] == 4:
        results['total_unused_ips'] = long(results['ipv4_total_ips']) - long(results['total_used_ips'])
        del results['ipv6_total_ips']

    if results['ip_family'] == 6:
        results['total_unused_ips'] = long(results['ipv6_total_ips']) - long(results['total_used_ips'])
        del results['ipv4_total_ips']

    del netblock

    return locals()


def freeIps():
    if request.vars.netblock_id:
        netblock_id = request.vars.netblock_id
    elif request.vars.id:
        netblock_id = request.vars.id
    else:
        return dict(message='Netblock ID was not supplied!')

    num_ips = 10
    if 'num_ips' in request.vars:
        num_ips = request.vars.num_ips

    netblock = db((db.netblock.id==netblock_id)&(db.netblock.netblock_type_id==db.netblock_type.id)).select(db.netblock.networkaddress,db.netblock_type.description).first()
    network_address = netblock['netblock']['networkaddress']
    netblock_type = netblock['netblock_type']['description']
    ipnw = IP(network_address)
    version = ipnw.version()
    prefix_length = ipnw.prefixlen()
    broadcast_address = ipnw.broadcast().strCompressed()

    if netblock_type != 'allocation':
        return dict(message="Netblock type must be allocation! Netblock ID: {0} is of type: {1}".format(netblock_id, netblock_type))

    family_bits = 32
    if version == 6:
        family_bits = 128
        if prefix_length < 72:
            original_prefix_length = prefix_length
            prefix_length = 72

    logger.debug("Netblock ID: {0} - Network Address: {1} - "
                 "Netblock Type: {2} - Version: {3} - "
                 "Prefix Length: {4} - Family bits: {5}".format(
        netblock_id,
        network_address,
        netblock_type,
        version,
        prefix_length,
        family_bits))

    # Here was the original query I found in message boards to allocated unused IP
    # SELECT sub.ip FROM (SELECT set_masklen(((generate_series(1, (2 ^ (32 -
    # masklen('192.0.0.0/8'::cidr)))::integer - 2) +
    # '192.0.0.0/8'::cidr)::inet), 32) as ip) AS sub WHERE sub.ip NOT IN
    # ('192.0.0.4'::inet, '192.0.0.8'::inet) LIMIT 10;

    sql = "SELECT host(sub.ip) FROM (SELECT set_masklen(((generate_series(1, (2 ^ ({3} - " \
        "{4}))::bigint - 2) + " \
        "'{0}'::cidr)::inet), {3}) as ip) AS sub WHERE sub.ip NOT IN " \
        "(SELECT ip FROM ip_address WHERE is_active = 'T' AND netblock_id = {1}) LIMIT {2}".format(
        network_address,
        netblock_id,
        num_ips,
        family_bits,
        prefix_length)

    results = db.executesql(sql)
    logger.debug(db._lastsql)

    del netblock
    del ipnw

    num_results = len(results)

    return locals()