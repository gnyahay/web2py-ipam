# -*- coding: utf-8 -*-


def index():
    return dict(message=T(''))


def user():
    """
    exposes:
    http://..../[app]/default/user/login
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    http://..../[app]/default/user/manage_users (requires membership in
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    """
    return dict(form=auth())


def call():
    """
    exposes services. for example:
    http://..../[app]/default/call/jsonrpc
    decorate with @services.jsonrpc the functions to expose
    supports xml, json, xmlrpc, jsonrpc, amfrpc, rss, csv
    """
    return service()


@request.restful()
def api():
    response.view = 'generic.'+request.extension
    def GET(*args,**vars):
        #import urllib
        #patterns = 'auto'
        patterns = [
            #api/locations.json
            '/locations[location]',
            # api/location/SAT5/network_segment.json
            '/location/{location.dc_abbr}/network_segments[network_segment.location_id]',

            #api/network_segment/pod1.sat5/netblocks.json
            '/network_segment/{network_segment.description}/netblocks[netblock.network_segment_id]',
            '/network_segment/{network_segment.id}/netblocks[netblock.network_segment_id]',

            #api/network_segment/pod1.sat5/vlans.json
            '/network_segment/{network_segment.description}/vlans[vlan.network_segment_id]',
            '/network_segment/{network_segment.id}/vlans[vlan.network_segment_id]',

            '/vlan/{vlan.id}',

            #api/vlans/9275/netblocks.json
            '/vlans[vlan]/{vlan.id}/netblocks[netblock.vlan_id]',
            '/vlans[vlan]/{vlan.description}/netblocks[netblock.vlan_id]',

            #api/vlan_types.json
            '/vlan_types[vlan_type]',

            '/netblock/{netblock.id}',

            #api/netblock/668/ips.json
            '/netblock/{netblock.id}/ips[ip_address.netblock_id]',

            '/netblock/owner/{netblock.owner}/ips[ip_address.netblock_id]',
            '/netblocks[netblock]/owner/{netblock.owner}',
            '/netblocks[netblock]/owner_type/{netblock.owner_type_id}/owner/{netblock.owner}',
            '/netblocks[netblock]/owner_type/{netblock.owner_type_id}/owner/{netblock.owner}/ips[ip_address.netblock_id]',

            '/ips[ip_address]/hostname/{ip_address.hostname}',
            '/ips[ip_address]/service_name/{ip_address.service_name}',

            '/ip[ip_address]/{ip_address.ip}',

            '/ip[ip_address]/{ip_address.ip}/list[ip_address_attribute.ip_address_id]/attributes[attribute.id]',
            '/ip_address[ip_address]/{ip_address.id}/list[ip_address_attribute.ip_address_id]/attributes[attribute.id]',

            #api/network_segments.json
            '/network_segments[network_segment]',

            #api/owner_types.json
            '/owner_types[owner_type]',

            #api/netblock_types.json
            '/netblock_types[netblock_type]',

            #api/attributes.json
            '/attributes[attribute]',

            '/ip_use_types[ip_address_use_type]',

            ]
        parser = db.parse_as_rest(patterns,args,vars)
        if parser.status == 200:
            return dict(content=parser.response)
        else:
            raise HTTP(parser.status,parser.error)
    def POST(table_name,**vars):
        return db[table_name].validate_and_insert(**vars)
    def PUT(table_name,record_id,**vars):
        return db(db[table_name]._id==record_id).update(**vars)
    def DELETE(table_name,record_id):
        return db(db[table_name]._id==record_id).delete()
    return locals()


def ajax():
    query = request.vars.key.split('-')
    table = query[0]
    if table == 'netseg':
        table = "network_segment"
    if table == 'ip':
        table = "ip_address"

    if table == 'netblock':
        key = "allocation-{0}".format(query[1])
        redirect(URL(c='netblock', f='ajax', vars={'key': key}))

    record = db[table](query[1])

    #db[table].id.readable = False
    db[table].created_on.readable = True
    db[table].created_by.readable = True
    db[table].modified_on.readable = True
    db[table].modified_by.readable = True

    form = SQLFORM(db[table], record, deletable=True, editable=True)

    if form.accepts(request):
        response.flash = 'Changes saved'

    return dict(form=form, table=table)


def ajaxAttributes():
    if request.vars.key:
        query = request.vars.key.split('-')
        table = query[0]
    else:
        return dict()
    if table == 'netseg':
        table = "network_segment"
    if table == 'ip':
        table = "ip_address"

    attributes_form = get_attributes_grid(db, table, query[1])
    return dict(form=attributes_form)


def ajaxHistory():
    if request.vars.key:
        query = request.vars.key.split('-')
        table = query[0]
    else:
        return dict()

    if table == 'netseg':
        table = "network_segment"
    if table == 'ip':
        table = "ip_address"

    history_form = get_table_history_grid(db, table, query[1])
    return dict(form=history_form)


def treeAjax():
    response.view = 'treeAjax.json'
    if request.vars['key']:
        query = request.vars.key.split('-')
        logger.debug(query)
        if query[0] == 'location':
            type = 'netseg'
            sql = """SELECT (%s || '-' || network_segment.id) AS key, description AS title, '../static/images/netseg-icon-32px.png' AS icon
                FROM network_segment
                WHERE network_segment.is_active = 'T'
                AND network_segment.location_id = %s
                ORDER BY description"""
        elif query[0] == 'netseg':
            type = "vlan"
            sql = """SELECT (%s || '-' || vlan.id) AS key, (vlan.vnumber || ' - ' || vlan_type.description) AS title, vlan.description AS tooltip, '../static/images/osa_hub.png' AS icon
                FROM vlan
                JOIN vlan_type ON vlan_type.id = vlan.vlan_type_id
                WHERE vlan.is_active = 'T'
                AND vlan.network_segment_id = %s
                ORDER BY vlan.vnumber"""
        elif query[0] == 'vlan':
            type = 'netblock'
            sql = """SELECT (%s || '-' || netblock.id) AS key, (netblock.networkaddress) AS title, '../static/images/network-icon-32px.png' AS icon
                FROM netblock
                WHERE netblock.is_active = 'T'
                AND netblock.vlan_id = %s
                ORDER BY netblock.networkaddress"""
        else:
            type = 'ip'
            sql = """SELECT (%s || '-' || ip_address.id) AS key, (ip_address.ip) AS title, '../static/images/osa_server.png' AS icon
                FROM ip_address
                WHERE ip_address.is_active = 'T'
                AND ip_address.netblock_id = %s
                ORDER BY ip_address.ip"""

        tree = db.executesql(sql, placeholders=[type, query[1]], as_dict=True)
    else:
        type = 'location'
        sql = """SELECT (%s || '-' || location.id) AS key, dc_abbr AS title, description AS tooltip, '../static/images/osa_site-head-office.png' AS icon
            FROM location
            WHERE location.is_active = 'T'
            ORDER BY dc_abbr"""
        tree = db.executesql(sql, placeholders=[type], as_dict=True)

    if type != 'ip':
         for node in tree:
             node['lazy'] = True
             node['folder'] = True

    return dict(tree=tree)