## Web2Py IP Address Management

Features:
- Track Locations -> Network Segments -> VLANs -> Netblocks -> IPs
- History records for all objects
- Custom attributes on all/most object types
- Statistics
- REST API

Directory Layout:
- ./ - Web2py framework
- ./applications/pips - IPAM App
- ./applications/pips/models/db.py - Configuration

Requirements:
- Python 2.7
- Postgres (To configure database connection string edit applications/pips/models/db.py)

Install Dependencies:
- pip install -r applications/*/requirements.txt

Running:
- python web2py.py

TODO:
- Fix IP address search
- Finish LDAP integration
